package kvstore.subcomponents

import akka.actor.ActorRef

case class Mail(receivers: Set[ActorRef], message: Any, resendable: Boolean = true)
case class Requester(from: ActorRef, id: Long)
case class Request(requester: Requester, mails: List[Mail])

case class RequestState(confirmees: Set[ActorRef], persisted: Boolean = false):
  def mkPersisted: RequestState = copy(persisted = true)
  def removeConfirmee(confirmee: ActorRef): RequestState = copy(confirmees = confirmees - confirmee)
  def addConfirmees(newConfirmees: Set[ActorRef]): RequestState = copy(confirmees = confirmees ++ newConfirmees)
  def isDone: Boolean = persisted && confirmees.isEmpty
