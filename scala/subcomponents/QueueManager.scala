package kvstore.subcomponents

import akka.actor.{ActorRef, Timers, actorRef2Scala}
import scala.concurrent.duration.*

trait TimeTicker extends Timers:
  protected var startedTime = 0L
  protected val Ticker = "Ticker"
  protected val Ticked = "Ticked"

  protected def currentTime: Long = System.currentTimeMillis()

  protected def startTicker(interval: FiniteDuration): Unit =
    startedTime = currentTime
    timers.startTimerAtFixedRate(Ticker, Ticked, interval)

  protected def stopTicker: Unit = timers.cancel(Ticker)

  protected def isTimeout(threshold: Long = 1100): Boolean =
    currentTime - startedTime > threshold
end TimeTicker

trait Dispatchable:
  protected def send: Unit
  protected def resend: Unit
  protected def next: Unit
end Dispatchable

trait QueueManager extends TimeTicker with Dispatchable:
  protected var queue = Vector.empty[Request]

  protected val tickInterval = 100.millis

  protected def enqueue(request: Request): Unit =
    queue = queue :+ request
    if queue.size == 1 then send

  protected def _send(mail: Mail): Unit =
    mail.receivers.foreach(_ ! mail.message)

  protected def send: Unit =
    queue.head.mails.foreach(_send)
    startTicker(tickInterval)

  protected def resend: Unit =
    if queue.nonEmpty then
      queue.head.mails.filter(_.resendable).foreach(_send)
    else stopTicker

  protected def next: Unit =
    stopTicker
    queue = queue.tail
    if queue.nonEmpty then send

  protected def respond(message: Any): Unit =
    queue.head.requester.from ! message

  // the request is being handled currently
  protected def request: Request = queue.head
end QueueManager
