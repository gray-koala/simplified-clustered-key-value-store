package kvstore.subcomponents

import kvstore.Replicator.Replicate
import akka.actor.{ActorRef, actorRef2Scala}

class InMemoryStore:
  private var _kv = Map.empty[String, String]

  def apply(key: String): Option[String] = _kv.get(key)

  def updated(key: String, valueOption: Option[String]): Unit =
    valueOption match
      case Some(value) => _kv += key -> value
      case None        => _kv -= key

  def syncWith(replicator: ActorRef)(implicit requester: ActorRef): Unit =
    for (((key, value), index) <- _kv.zip(LazyList from 0))
      replicator.tell(Replicate(key, Some(value), index), requester)
