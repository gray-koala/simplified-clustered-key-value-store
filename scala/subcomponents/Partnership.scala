package kvstore.subcomponents

import akka.actor.ActorRef

// secondary -> its replicator
class Partnership:
  private var _map = Map.empty[ActorRef, ActorRef]

  def apply(keys: Set[ActorRef]): Map[ActorRef, ActorRef] =
    _map.view.filterKeys(keys.contains(_)).toMap

  def replicators: Set[ActorRef] = _map.values.toSet

  def secondaries: Set[ActorRef] = _map.keySet

  def add(secondary: ActorRef, replicator: ActorRef): Unit =
    _map += secondary -> replicator

  def remove(secondaries: Set[ActorRef]): Unit =
    _map = _map -- secondaries
