package kvstore.subcomponents

class Counter:
  private var _seq = 0L

  def seq: Long = _seq

  def increment: Unit = _seq += 1

  def nextSeq: Long =
    val ret = seq
    increment
    ret
