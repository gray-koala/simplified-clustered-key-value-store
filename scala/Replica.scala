package kvstore

import akka.actor.{OneForOneStrategy, PoisonPill, Props, SupervisorStrategy, Terminated, ActorRef, Actor, actorRef2Scala}
import kvstore.Arbiter.*
import kvstore.subcomponents.*
import scala.concurrent.duration.*
import akka.util.Timeout

object Replica:
  sealed trait Operation:
    def key: String
    def id: Long
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(Replica(arbiter, persistenceProps))

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor with QueueManager:
  import Replica.*
  import Replicator.*
  import Persistence.*
  import context.dispatcher

  arbiter ! Join

  val persistence = context.actorOf(persistenceProps)
  val store = new InMemoryStore

  override val supervisorStrategy = OneForOneStrategy() {
      case _: Exception => SupervisorStrategy.Restart
    }

  def receive =
    case JoinedPrimary   => context.become(Primay.behavior(RequestState(Set.empty)))
    case JoinedSecondary => context.become(Secondary.behavior)

  object Primay:
    val partnership = new Partnership

    def behavior(state: RequestState): Receive =
      case message: (Insert | Remove) =>
        val (key, valueOption, id) = message match
          case Insert(key, value, id) => (key, Some(value), id)
          case Remove(key, id) => (key, None, id)
        store.updated(key, valueOption)
        val persistMail = Mail(Set(persistence), Persist(key, valueOption, id))
        val replicateMail = Mail(partnership.replicators, Replicate(key, valueOption, id), resendable = false)
        enqueue(Request(Requester(sender, id), List(persistMail, replicateMail)))
      case Get(key, id) => sender ! GetResult(key, store(key), id)
      case message: (Persisted | Replicated | Terminated) =>
        val newState = message match
          case _: Persisted if !state.persisted => state.mkPersisted
          case Replicated(_, id) if queue.nonEmpty && (request.requester.id == id) => state.removeConfirmee(sender)
          case Terminated(stopped) if queue.nonEmpty => state.removeConfirmee(stopped)
          case _ => state
        if newState.isDone then
          context.become(behavior(RequestState(partnership.replicators)), discardOld = true)
          respond(OperationAck(request.requester.id))
          next
        else context.become(behavior(newState), discardOld = true)
      case Ticked =>
        if isTimeout() then
          context.become(behavior(RequestState(partnership.replicators)), discardOld = true)
          respond(OperationFailed(request.requester.id))
          next
        else resend
      case Replicas(replicas) =>
        val changed = replicas - self
        val newJoiners = changed -- partnership.secondaries
        newJoiners.foreach { secondary =>
            val replicator = context.actorOf(Replicator.props(secondary))
            partnership.add(secondary, replicator)
            context.watch(replicator)
            store.syncWith(replicator)
          }
        val newRepliators = partnership(newJoiners).values.toSet
        if newRepliators.nonEmpty then
          context.become(behavior(state.addConfirmees(newRepliators)), discardOld = true)
        val leavings = partnership.secondaries -- changed
        partnership(leavings).foreach { case (secondary, replicator) =>
            secondary ! PoisonPill
            replicator ! PoisonPill
          }
        partnership.remove(leavings)
  end Primay

  object Secondary:
    val counter = new Counter

    def behavior: Receive =
      case Get(key, id) => sender ! GetResult(key, store(key), id)
      case Snapshot(key, valueOption, seq) =>
        (seq compare counter.seq) match
          case 0 if queue.isEmpty =>
            store.updated(key, valueOption)
            val mail = Mail(Set(persistence), Persist(key, valueOption, seq))
            enqueue(Request(Requester(sender, seq), List(mail)))
          case cmp if cmp < 0 => sender ! SnapshotAck(key, seq)
          case _ => //ingore
      case Ticked => resend
      case Persisted(key, id) =>
        counter.increment
        respond(SnapshotAck(key, id))
        next
  end Secondary
end Replica
