package kvstore

import akka.actor.{Props, Actor, ActorRef, actorRef2Scala}
import scala.concurrent.duration.*
import kvstore.subcomponents.{QueueManager, Request, Counter, Mail, Requester}

object Replicator:
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)

  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(Replicator(replica))

class Replicator(val replica: ActorRef) extends Actor with QueueManager:
  import Replicator.*
  import context.dispatcher

  val counter = new Counter

  def receive: Receive =
    case Replicate(key, valueOption, id) =>
      val mail = Mail(Set(replica), Snapshot(key, valueOption, counter.nextSeq))
      enqueue(Request(Requester(sender, id), List(mail)))
    case SnapshotAck(key, seq) =>
      if queue.nonEmpty then
        respond(Replicated(key, request.requester.id))
        next
    case Ticked => resend
